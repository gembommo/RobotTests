*** Settings ***
Documentation     A test suite that contains 3 tests, each one is printing to clonsole an other user's param..

*** Variables ***
${param1}	
${param2}		
${param3}		

*** Test Case ***
print param 1
	print    ${param1}
	
print param 2
	print    ${param2}
	
print param 3
	print    ${param3}

*** Keywords ***
	
print
	[Arguments]    ${param}
	Log To Console  ${param}  DEBUG
