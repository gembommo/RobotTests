def get_variables(is_good_world:bool):
    variables = { "WORLD": "Nice world" } if is_good_world else { "WORLD": "Cruel world" }
    return variables