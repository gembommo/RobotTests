*** Settings ***
Documentation     get sandbox as dict
Library    Collections

*** Test Case ***
Print sandbox id
	X        ${sandbox_dict['id']}
	Y		 ${sandbox_dict['global_inputs']}

*** Keywords ***
X
	[Arguments]    ${id}
    Log To Console			${id}
Y
	[Arguments]    ${global_inputs}
	${num} = 	Get From List 	${global_inputs} 	1
    Log To Console			${num['value']}
